﻿
namespace ExercicioCronometro
{
    using System;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        private readonly Cronometro cronometro;
        public Form1()
        {
            InitializeComponent();
            cronometro = new Cronometro();
        }

        private void buttonOnOff_Click(object sender, EventArgs e)
        {
            if (cronometro.ClockState())
            {
                cronometro.StopClock();
                buttonOnOff.Text = "Liga";
                timerRelogio.Enabled = false;
                // label1.Text = cronometro.GetTimeSpan().ToString();
            }
            else
            {
                cronometro.StartClock();
                buttonOnOff.Text = "Desliga";
                timerRelogio.Enabled = true;

            }
        }
        private void UpdateLabel()
        {
            var tempo = DateTime.Now - cronometro.StartTime();
            label1.Text = string.Format("{0:D2}:{1:D2}:{2:D2}:{3:D2}",tempo.Hours, tempo.Minutes,tempo.Seconds,tempo.Milliseconds);
        }

        private void timerRelogio_Tick(object sender, EventArgs e)
        {
            UpdateLabel();
        }
    }
}
