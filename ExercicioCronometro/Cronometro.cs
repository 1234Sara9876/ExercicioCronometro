﻿

namespace ExercicioCronometro
{
    using System;
    public class Cronometro
    {
        #region atributos

        private DateTime start;
        private DateTime stop;
        private bool isrunning;

        #endregion
        //n é preciso nem propriedades (pq os atributos n vão sair da classe) nem construtor(??)

        public void StartClock()
        {
            if (isrunning)
                throw new InvalidOperationException("O cronómetro já está ligado!");
            start = DateTime.Now;
            isrunning = true;
        }
        public void StopClock()
        {
            if (!isrunning)
                throw new InvalidOperationException("O cronómetro já está ligado!");
            stop = DateTime.Now;
            isrunning = false;
        }
        public TimeSpan GetTimeSpan()
        {
            return stop - start;
        }
        public bool ClockState()
        {
           return isrunning;
        }
        public DateTime StartTime()
        {
            return start;
        }

    }
}
